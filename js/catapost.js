/*global catapost, tb_show, tb_remove */
jQuery(document).ready(function($){
	
	$.fn.exists = function()
	{
		return $(this).length>0;
	};
    
	$('.catapost-file-uploader, .catapost-image-uploader').each(function()
	{
		// add file
		$('.add-file', this).live('click', function(e)
		{
			e.preventDefault();
			
			var $div     = $(this).closest('.catapost-uploader'),
				$field   = $div.children('input[type=hidden]'),
				$preview = $div.find('img'),
				type     = ( $div.is('.catapost-image-uploader') ) ? 'image' : 'file';
			
			// show the thickbox
			tb_show( 'Add File to Field', catapost.url +'/media-upload.php?post_id='+ catapost.post_id +'&catapost_type='+ type +'&type='+ type +'&TB_iframe=1' );
			
			// Replace the insert into post updater script
			window.send_to_editor = function(html){
				var file_url, id;
				
				if ( 'image' === type ) {
					file_url = $('img', html).attr('src');
					id       = $('img', html).attr('class').replace(/(.*?)wp-image-/, '');
						
					$field.val(id);
					$preview.attr('src', file_url);
				} else {
					file_url = $(html).attr('href');
					id       = $(html).attr('class').replace(/(.*?)catapost-file-/, '');
					var mime     = $(html).attr('data-mime'),
						icon     = $('img', html).attr('src'),
						name     = $(html).attr('title');
						
					// Store in input to add to database
					$field.val(id);
					// update file mime type icon
					$div.find('.file-icon').attr('src', icon);
					// Add file name to title
					$div.find('.file-name').text(name);
				}
				$div.addClass('active');
				tb_remove();
			};
		});
		
		// remove file
		$('.remove-file', this).live('click', function(e)
		{
			e.preventDefault();
			
			var $div = $(this).closest('.catapost-uploader');
			
			$div.removeClass('active').find('input[type=hidden]').val('');
		});
		
		// edit file
		$('.edit-file', this).live('click', function(e)
		{
			e.preventDefault();
			
			var $div = $(this).closest('.catapost-uploader'),
				id   = $div.find('input[type=hidden]').val(),
				type = ( $div.is('.catapost-image-uploader') ) ? 'image' : 'file';
			
			// show edit attachment
			tb_show( 'Edit File', catapost.url + '/media.php?attachment_id='+ id +'&action=edit&catapost_action=edit_attachment&catapost_field='+ type +'&TB_iframe=1');
		});
	});
	
});
