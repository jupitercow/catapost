<?php

/**
 * Init Setup
 *
 * Gets everything setup and loaded. This should be the only file that needs to be loaded in.
 *
 * @package catapost
 */

require_once( 'core/catapost.php' );
require_once( 'core/catapost_meta_box.php' );
require_once( 'core/catapost_post_type.php' );
require_once( 'core/catapost_front_end.php' );

?>