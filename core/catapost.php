<?php

/**
 * Base Support
 *
 * Basic stuff like prefix and text domain setup
 *
 * @package catapost
 */

class catapost
{
	var $version,
		$html_prefix,
		$prefix,
		$db_prefix,
		$text_domain,
		$field_namespace;
	
	public function __construct()
	{
		// Catapost Version
		$this->version = '0.2.7';
		
		$this->field_namespace = '\\catapost\\field\\';
		
		// The prefixes that get used, this is partly so they can be overwritten using filters to personalize themes.
		$this->set_prefix();
		$this->set_db_prefix();
		$this->set_html_prefix();
		
		// The translation domain can be changed to match a theme.
		$this->set_text_domain();
		
		// Always register the scripts for later use
		add_action( 'init', array($this, 'register_scripts_and_styles') );
		
		// Always enqueue them on admin pages that need them (post & post-new), but don't enqueue anywhere else unless specified later.
		if ( $this->validate_admin_pages() ) add_action( 'admin_enqueue_scripts', array($this, 'enqueue_scripts_and_styles') );
	}
	
	/**
	 * Settings: Version
	 *
	 * Current version
	 *
     * @return filtered version
	 */
	public function version()
	{
		return apply_filters( $this->prefix .'_'. __FUNCTION__, $this->version );
	}
	
	/**
	 * Settings: Directory Path
	 *
	 * The path beyond theme stylesheet diretory, filtered. Mainly for use with directory functions below.
	 *
     * @return filtered directory
	 */
	public function setup_directory_path( $path='' )
	{
		return apply_filters( $this->prefix .'_'. __FUNCTION__, '/'. $this->prefix . (! empty($path) ? '/'. $path : '') );
	}
	
	/**
	 * Settings: Directory, URI
	 *
	 * Directory catapost is stored in
	 *
     * @return filtered directory
	 */
	public function directory_uri( $path='' )
	{
		return apply_filters( $this->prefix .'_'. __FUNCTION__, get_stylesheet_directory_uri() . $this->setup_directory_path($path) );
	}
	
	/**
	 * Settings: Directory, Absolute
	 *
	 * Directory catapost is stored in
	 *
     * @return filtered directory
	 */
	public function directory( $path='' )
	{
		return apply_filters( $this->prefix .'_'. __FUNCTION__, get_stylesheet_directory() . $this->setup_directory_path($path) );
	}
	
	/**
	 * Settings: jQuery UI Theme
	 *
	 * The key for a jquery ui theme stored in the Google API
	 *
	 * Options:
	 * base, black-tie, blitzer, cupertino, dark-hive, dot-luv, eggplant, excite-bike, flick, hot-sneaks, humanity, le-frog, mint-choc, overcast, pepper-grinder, redmond, smoothness, south-street, start, sunny, swanky-purse, trontastic, ui-darkness, ui-lightness, vader
	 *
     * @return filtered theme
	 */
	public function jquery_ui_theme()
	{
		return apply_filters( $this->prefix .'_'. __FUNCTION__, 'smoothness' );
	}
	
	/**
	 * Set: Prefix
	 *
	 * The prefix is used in the class as a prefix for things filters, etc.
	 *
     * @param string $string prefix text
     * @return void
	 */
	public function set_prefix( $string='' )
	{
		if ( empty($string) ) $string = __CLASS__;
		$this->prefix = apply_filters( __CLASS__ .'_'. __FUNCTION__, $this->key_format($string) );
	}
	
	/**
	 * Set: Database Prefix
	 *
	 * The db prefix is used to store info in the database.
	 *
     * @param string $string prefix text
     * @return void
	 */
	public function set_db_prefix( $string='' )
	{
		if ( empty($string) ) $string = __CLASS__;
		$this->db_prefix = apply_filters( $this->prefix .'_'. __FUNCTION__, $this->key_format($string) );
	}
	
	/**
	 * Set: HTML Prefix
	 *
	 * The html prefix is used for html output which affects styling mainly.
	 *
     * @param string $string prefix text
     * @return void
	 */
	public function set_html_prefix( $string='' )
	{
		if ( empty($string) ) $string = __CLASS__;
		$this->html_prefix = apply_filters( $this->prefix .'_'. __FUNCTION__, $this->key_format($string) );
	}
	
	/**
	 * Set: Domain
	 *
	 * This is the text domain used for translation. The default is the base prefix (catapost), but can be changed in settings to
	 * use the theme's translation domain.
	 *
     * @return void
	 */
	public function set_text_domain()
	{
		$this->text_domain = apply_filters( $this->prefix .'_'. __FUNCTION__, $this->prefix );
	}
	
	/**
	 * Local Domain Translation
	 *
	 * Return translation from local domain.
	 *
     * @param string $type the text to be run through tranlation
     * @return string translated text
	 */
	public function __( $string )
	{
		return __($string, $this->text_domain);
	}
	
	/**
	 * Local Domain Translation
	 *
	 * Echo translation from local domain.
	 *
     * @param string $string the text to be run through tranlation
     * @return void
	 */
	public function _e( $string )
	{
		return _e($string, $this->text_domain);
	}
	
	/**
	 * Format: Title
	 *
	 * Used to convert underscores to spaces and make sure all words are capitalized.
	 *
     * @param string $string the title to format
     * @return void
	 */
	public function title_format( $string )
	{
		return ucwords( sanitize_text_field(str_replace( '_', ' ', $string )) );
	}
	
	/**
	 * Format: Key or Class
	 *
	 * Used to create a sanitized, simple version, alpha-numeric without spaces.
	 *
     * @param string $string the key or class to format
     * @return void
	 */
	public function key_format( $string )
	{
		return sanitize_title($string);
	}
	
	/**
	 * Format: Meta Key
	 *
	 * Takes the provided name, runs it through key_format, and adds the prefix- to the front.
	 *
     * @param string $name the name to use, probably a title
     * @return $string newly formatted title with prefix
	 */
	public function meta_key( $name )
	{
		return $this->db_prefix .'-'. $this->key_format($name);
	}
	
	/**
	 * New NONCE functions
	 *
	 */
	public function nonce_name( $name )
	{
		return $this->prefix .'-'. $name .'-nonce';
	}
	
	public function nonce_action( $action )
	{
		return $this->prefix .'-'. $action;
	}
	
	public function create_nonce( $action )
	{
		wp_create_nonce( $this->nonce_action($action) );
	}
	
	public function nonce_field( $action, $name )
	{
		wp_nonce_field( $this->nonce_action($action), $this->nonce_name($name) );
	}
	
	public function verify_nonce( $action, $name )
	{
		$nonce_name   = $this->nonce_name($name);
		if ( empty($_POST[$nonce_name]) || ! wp_verify_nonce($_POST[$nonce_name], $this->nonce_action($action)) )
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Validate Admin Pages
	 *
	 * Validate the admin pages to make sure scripts and styles are only included on necessary pages
	 *
	 * @return boolean
	 */
	public function validate_admin_pages()
	{
		global $pagenow, $typenow;
		
		if ( in_array($pagenow, array('post.php', 'post-new.php')) )
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Front end form setup
	 *
	 * This gets the form support scripts and styles ready to be loaded if needed by form, so they, ideally, aren't loaded when not needed.
	 *
	 * @return void
	 */
	public function form_init()
	{
		// Add admin scripts and styles
		add_action( 'wp_head',  array($this, 'enqueue_scripts_and_styles') );
	}
	
	/**
	 * Register Scripts & Styles
	 *
	 * @return void
	 */
	public function register_scripts_and_styles()
	{
		// Custom styles
		wp_register_style( 'catapost-css', $this->directory_uri( 'css/catapost.css' ), array(), $this->version(), 'all' );
		// jQuery UI theme styles, register for use when a field is using jquery ui
		wp_register_style( 'catapost-jquery-ui-theme', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/'. $this->jquery_ui_theme() .'/jquery-ui.css', array(), $this->version(), 'all' );
		
		// Custom scripts
		wp_register_script( 'catapost-js', $this->directory_uri( 'js/catapost.js' ), array( 'jquery' ), $this->version(), true );
		
		do_action( 'register_catapost_scripts_and_styles' );
	}
	
	/**
	 * Enqueue Scripts & Styles
	 *
	 * @return void
	 */
	public function enqueue_scripts_and_styles()
	{
		global $post;
		
		wp_enqueue_style(array(
			'thickbox',
			'catapost-css'
		));
		
		$args = array(
			'nonce'       => wp_create_nonce( 'catapost-ajax-nonce' ),
			'spinner'     => admin_url( 'images/loading.gif' ),
			'url'         => admin_url( 'admin-ajax.php' ),
			'post_id'     => (! empty($post->ID) ? $post->ID : 0)
		);
		wp_localize_script( 'catapost-js', 'catapost', apply_filters( 'catapost-js-localize', $args ) );
		wp_enqueue_script(array(
			'jquery',
			'thickbox',
			'media-upload',
			'catapost-js'
		));
		
		do_action( 'catapost_enqueue_scripts_and_styles' );
	}
		
	/**
	 * Advanced Pluralize
	 */
	public function pluralize( $string )
	{
		$plurals = array(
			'/(quiz)$/i'                     => "$1zes",
			'/^(ox)$/i'                      => "$1en",
			'/([m|l])ouse$/i'                => "$1ice",
			'/(matr|vert|ind)ix|ex$/i'       => "$1ices",
			'/(x|ch|ss|sh)$/i'               => "$1es",
			'/([^aeiouy]|qu)y$/i'            => "$1ies",
			'/(hive)$/i'                     => "$1s",
			'/(?:([^f])fe|([lr])f)$/i'       => "$1$2ves",
			'/(shea|lea|loa|thie)f$/i'       => "$1ves",
			'/sis$/i'                        => "ses",
			'/([ti])um$/i'                   => "$1a",
			'/(tomat|potat|ech|her|vet)o$/i' => "$1oes",
			'/(bu)s$/i'                      => "$1ses",
			'/(alias)$/i'                    => "$1es",
			'/(octop)us$/i'                  => "$1i",
			'/(ax|test)is$/i'                => "$1es",
			'/(us)$/i'                       => "$1es",
			'/s$/i'                          => "s",
			'/$/'                            => "s"
		);
		
		$irregulars = array(
			'move'   => 'moves',
			'foot'   => 'feet',
			'goose'  => 'geese',
			'sex'    => 'sexes',
			'child'  => 'children',
			'man'    => 'men',
			'tooth'  => 'teeth',
			'person' => 'people'
		);
		
		$noplural = array('sheep','fish','deer','series','species','money','rice','information','equipment');
		
		$string_low = strtolower($string);
		
		// If plural and singular are the same, done
		if ( in_array($string_low, $noplural) ) return $string;
		
		// If irregular singular, use irregular pluralize
		foreach ( $irregulars as $key => $result )
		{
			if( substr($string_low, -strlen($key)) === $key ) return rtrim($string, $key) . $result;
		}
		
		// Use regular expressions to run the basics
		foreach ( $plurals as $pattern => $result )
		{
			if ( preg_match($pattern, $string) ) return preg_replace($pattern, $result, $string);
		}
		
		return $string;
	}
}
