<?php

/**
 * Generic Field Support
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class field extends \catapost
{
	var $type;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->type = str_replace(__NAMESPACE__ .'\\', '', get_class($this));
		
		// Enqueue each fields scripts and styles when loaded
		if ( method_exists($this, 'field_scripts_and_styles') )
		{
			add_action( $this->prefix .'_enqueue_scripts_and_styles', array($this, 'field_scripts_and_styles') );
		}
		
		if (! has_action( $this->prefix .'_show_field_'. $this->type ) )
		{
			add_action( $this->prefix .'_show_field_'. $this->type, array($this, 'show_field'), 10, 1 );
		}
		
		if (! has_action( $this->prefix .'_save_field' ) )
		{
			add_action( $this->prefix .'_save_field', array($this, 'save_field'), 10, 2 );
		}
	}
	
	public function create( $meta_key, $value, $settings )
	{
		
	}
	
	public function get_default_value( $post_id, $meta_key )
	{
		return get_post_meta( $post_id, $meta_key, true );
	}
	
	public function show_field( $args=array() )
	{
		if (! empty($args['post_id']) && is_numeric($args['post_id']) )
		{
			$post = get_post($args['post_id']);
		}
		else
		{
			global $post;
		}
		
		$defaults = array(
			'type'     => 'text',
			'settings' =>  array()
		);
		$args = array_merge( $defaults, $args );
		extract($args);
		
		if (! empty($name) )
		{
			$meta_key = (! empty($meta_key) ) ? $meta_key : $this->meta_key($name);
			
			if (! isset($value) )
			{
				$value = (! empty($args['show_default_value']) ) ? $this->get_default_value( $post->ID, $meta_key ) : '';
			}
			
			if ( 'hidden' != $type )
			{
				?> 
				<div id="<?php echo $meta_key ?>-field" class="<?php echo $this->prefix; ?>-field <?php echo $this->prefix; ?>-field-<?php echo $type; ?> clearfix">
					<p class="label">
						<?php $this->formblock_label($name, $meta_key); ?></label>
						<?php if (! empty($description) ) echo $description; ?> 
					</p>
				<?php
			}
			
			// Get the field
			if ( isset($args['show_default_value']) ) $settings['show_default_value'] = $args['show_default_value'];
			$settings['post_id'] = $post->ID;
			$this->create($meta_key, $value, $settings);
			
			if ( 'hidden' != $type ) echo '</div>';
		}
	}
	
	public function save_field( $post_id, $field_name )
	{
		$meta_key   = $this->meta_key($field_name);
		/** /
		$prev_value = get_post_meta($post_id, $meta_key, true);
		$meta_value = ( isset($_POST[$meta_key]) ) ? $_POST[$meta_key] : false;
		
		/** /
		if ( false !== $meta_value && $meta_value != $prev_value )
		{
			update_post_meta( $post_id, $meta_key, $meta_value );
		}
		elseif ( empty($meta_value) && ! empty($prev_value) )
		{
			delete_post_meta( $post_id, $meta_key, $prev_value );
		}
		/** /
		if (! empty($meta_value) && $meta_value != $prev_value )
		{
			update_post_meta( $post_id, $meta_key, $meta_value );
		}
		elseif ( empty($meta_value) && ! empty($prev_value) )
		{
			delete_post_meta( $post_id, $meta_key, $prev_value );
		}
		/**/
		
		if ( isset($_POST[$meta_key]) )
		{
			$prev_value = get_post_meta($post_id, $meta_key, true);
			$meta_value = ( isset($_POST[$meta_key]) ) ? $_POST[$meta_key] : false;
			
			if ( (! empty($meta_value) || 0 === $meta_value) && $meta_value != $prev_value )
			{
				update_post_meta( $post_id, $meta_key, $meta_value );
			}
			elseif ( empty($meta_value) && 0 !== $meta_value && ! empty($prev_value) )
			{
				delete_post_meta( $post_id, $meta_key, $prev_value );
			}
		}
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	*	Form Building Blocks and Field Types
	*
	*	@author Jake Snyder
	*	@since 0.0.1
	* 
	*-------------------------------------------------------------------------------------*/
	
	
	/**
	 * Block: Type
	 *
	 * Creates an input field's type and class attribute out of the field type.
	 *
     * @param string $type the field type which will get duplicated as a class
     * @return void
	 */
	public function formblock_type( $type )
	{
		$type = esc_attr($type);
		if (! empty($type) ) echo ' type="'. $type .'" class="'. $type .'"';
	}
	
	/**
	 * Block: Name and Id
	 *
	 * Creates an name and id attributes with one string input that gets duplicated for both.
	 *
     * @param string $meta_key used as the name and id attributes
     * @return void
	 */
	public function formblock_nameid( $meta_key )
	{
		$name = esc_attr($meta_key);
		if (! empty($meta_key) ) echo ' name="'. $meta_key .'" id="'. $meta_key .'"';
	}
	
	/**
	 * Block: Value
	 *
	 * Creates value attribute, it runs a check to see if the submitted value is empty or not before adding it.
	 *
     * @param string $value the field type which will get duplicated as a class
     * @return void
	 */
	public function formblock_value( $value )
	{
		if ( is_string($value) || is_numeric($value) ) echo ' value="'. esc_attr($value) .'"';
	}
	
	/**
	 * Block: Label
	 *
	 * Creates a label tag. If "for" is submitted, it will add a for attribute. For attribute is left out by default
	 *
     * @param string $label the tag's labe text
     * @param string $for the id of the input or select that this label connects to, not required
     * @return void
	 */
	public function formblock_label( $label, $for='' )
	{
		if (! empty($label) ) echo '<label'. (! empty($for) ? ' for="'. esc_attr($for) .'"' : '') .'>'. esc_html($label) .'</label>';
	}
	
	/**
	 * Block: Checked
	 *
	 * Takes a true/false event and echos checked if true and nothing if false
	 *
	 * Examples:
	 *     in_array($value, $array)
	 *     $something == $somethingelse
	 *
     * @param boolean $true_false something that outputs true or false to decide if checked should be shown
     * @return void
	 */
	public function formblock_checked( $true_false )
	{
		if ( $true_false ) echo ' checked="yes"';
	}
	
	/**
	 * Block: Selected
	 *
	 * Takes a true/false event and echos selected if true and nothing if false
	 *
	 * Examples:
	 *     in_array($value, $array)
	 *     $something == $somethingelse
	 *
     * @param boolean $true_false something that outputs true or false to decide if checked should be shown
     * @return void
	 */
	public function formblock_selected( $true_false )
	{
		if ( $true_false ) echo ' selected="yes"';
	}
	
	/**
	 * Block: Input
	 *
	 * Creates a somewhat generic input tag, though it allows checkbox use as well
	 *
     * @param string $type input type attribute, eg: text, submit, img, checkbox ...
     * @param string $meta_key the id/name attributes of input
     * @param string $value value attribute, not required
     * @param string $option_value the value of an option when creating a checkbox and value is checkbox array values, not required
     * @return void
	 */
	public function formblock_input( $type, $meta_key, $value='', $option_value='' )
	{
		if ( $type && $meta_key )
		{
			echo '<input';
			
			$this->formblock_type($type);
			
			if ( 'checkbox' == $type || 'radio' == $type )
			{
				echo ' name="'. $meta_key . ('checkbox' == $type ? '[]' : '') .'" id="'. $meta_key .'-'. $this->key_format($option_value) .'"';
				$this->formblock_value($option_value);
				
				$true_false = ('checkbox' == $type && is_array($value) ) ? in_array($option_value, $value) : $option_value == $value;
				$this->formblock_checked($true_false);
			}
			else
			{
				$this->formblock_nameid($meta_key);
				$this->formblock_value($value);
			}
			
			echo ' />';
		}
	}
	
	/**
	 * Block: Box for Radio and Checkboxes
	 *
	 * Used to create radio and checkbox lists
	 *
     * @param string $type input type attribute, eg: radio or checkbox ...
     * @param string $meta_key the id/name attributes of input
     * @param string $value the stored array of selected values, not required
     * @param string $option_name the title of this option shown next to radio/checkbox
     * @param string $option_value the value of an option when selected, if nothing is provided the option_name will be used, not required
     * @return void
	 */
	public function formblock_box( $type, $meta_key, $value='', $option_name='', $option_value='' )
	{
		if ( empty($option_value) ) $option_value = $option_name;
		$for = ( 'checkbox' == $type || 'radio' == $type ) ? $meta_key .'-'. $this->key_format($option_value) : $meta_key;
		?> 
		<li>
			<label for="<?php echo esc_attr($for); ?>"><?php $this->formblock_input($type, $meta_key, $value, $option_value); ?> <?php echo esc_html($option_name); ?></label>
		</li>
		<?php
	}
	
	/**
	 * Block: Option for Select
	 *
	 * Used to create options lists for select elements
	 *
     * @param string $meta_key the id/name attributes of input
     * @param string $value the stored array of selected values, not required
     * @param string $option_name the title of this option
     * @param string $option_value the value of an option when selected, if nothing is provided the option_name will be used, not required
     * @return void
	 */
	public function formblock_option( $meta_key, $value='', $option_name='', $option_value='' )
	{
		//if ( empty($option_value) ) $option_value = $option_name;
		#echo '$option_value = '. $option_value .', value = '. $value;
		$selected = ( is_array($value) ) ? in_array($option_value, $value) : ( false !== $value && $option_value == $value);
		
		?> 
		<option value="<?php echo esc_attr($option_value); ?>"<?php $this->formblock_selected($selected); ?>>
			<?php esc_html_e($option_name); ?> 
		</option>
		<?php
	}
}
