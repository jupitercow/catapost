<?php

/**
 * Custom Post Type Class
 * 
 * Helps create and manage custom post types
 * 
 * @package catapost
 * @subpackage catapost_post_type
 */

class catapost_post_type extends catapost
{
	var $post_type,
		$title,
		$plural,
		$slug,
		$args,
		$labels,
		$taxonomy_title,
		$taxonomy_plural,
		$taxonomy_slug,
		$taxonomy_args,
		$taxonomy_labels;
	
	static function get_instance()
	{
		static $instance;
		return $instance ? $instance : $instance = new self;
	}
	
	private function __clone() {}
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function add_post_type( $name, $args=array(), $labels=array() )
	{
		if (! empty($name) )
		{
			$this->title  = apply_filters( __CLASS__ .'_title',  $this->title_format($name) );
			$this->plural = apply_filters( __CLASS__ .'_plural', $this->pluralize($this->title) );
			$this->slug   = apply_filters( __CLASS__ .'_slug',   $this->key_format($this->title) );
			$this->args   = apply_filters( __CLASS__ .'_args',   $args );
			$this->labels = apply_filters( __CLASS__ .'_labels', $labels );
			
			// Add action to register the post type, if the post type doesn't exist
			add_action( 'init', array($this, 'register_post_type') );
		}
	}
	
	/**
	 * Register the post type
	 *
	 * @return post type object or error object
	 */
	public function register_post_type()
	{
		if (! post_type_exists( $this->slug ) )
		{
			// Create labels based on the provided name and plural
			$default_labels = array(
				'name'                => $this->__( $this->plural ), // This is the Title of the Group
				'singular_name'       => $this->__( $this->title ), // This is the individual type
				'all_items'           => $this->__( 'All '. $this->plural ), // the all items menu item
				'add_new'             => $this->__( 'Add New' ), // The add new menu item
				'add_new_item'        => $this->__( 'Add New '. $this->title ), // Add New Display Title
				'edit'                => $this->__( 'Edit' ), // Edit Dialog
				'edit_item'           => $this->__( 'Edit '. $this->title ), // Edit Display Title
				'new_item'            => $this->__( 'New '. $this->title ), // New Display Title
				'view_item'           => $this->__( 'View '. $this->title ), // View Display Title
				'search_items'        => $this->__( 'Search '. $this->title ), // Search Custom Type Title
				'not_found'           => $this->__( 'Nothing found in the Database.' ), // This displays if there are no entries yet
				'not_found_in_trash'  => $this->__( 'Nothing found in Trash' ), // This displays if there is nothing in the trash
				'parent_item_colon'   => ''
			);
			$labels = array_merge( $default_labels, $this->labels );
			
			// Setup default arguments
			$default_args = array(
				'labels'              => $labels,
				'public'              => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'show_ui'             => true,
				'query_var'           => true,
				'menu_position'       => 8, // this is what order you want it to appear in on the left hand side menu
				'menu_icon'           => $this->directory_uri( 'images/custom-post-icon.png' ), // the icon for the custom post type menu
				'rewrite'	          => array( 'slug' => $this->slug, 'with_front' => false ), // you can specify it's url slug
				'has_archive'         => $this->key_format( $this->plural ), // you can rename the archive slug here
				'capability_type'     => 'post',
				'hierarchical'        => false,
				// the next one is important, it tells what's enabled in the post editor
				'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
			);
			$args = array_merge( $default_args, $this->args );
			
			return register_post_type( $this->slug, $args );
		}
	}
	
	public function add_taxonomy( $name, $args=array(), $labels=array() )
	{
		if (! empty($name) )
		{
			$this->taxonomy_title  = apply_filters( __CLASS__ .'_taxonomy_title',  $this->title_format($name) );
			$this->taxonomy_plural = apply_filters( __CLASS__ .'_taxonomy_plural', $this->pluralize($this->taxonomy_title) );
			$this->taxonomy_slug   = apply_filters( __CLASS__ .'_taxonomy_slug',   $this->key_format($this->taxonomy_title) );
			$this->taxonomy_args   = apply_filters( __CLASS__ .'_taxonomy_args',   $args );
			$this->taxonomy_labels = apply_filters( __CLASS__ .'_taxonomy_labels', $labels );
			
			// Add action to register the post type, if the post type doesn't exist
			#add_action( 'init', array($this, 'register_taxonomy') );
			$this->register_taxonomy();
		}
	}
	
	/**
	 * Attach a taxonomy to the post type
	 */
	public function register_taxonomy()
	{
		if (! empty($this->taxonomy_slug) )
		{
			$slug      = $this->taxonomy_slug;
			$post_type = $this->slug;
			
			if (! taxonomy_exists( sanitize_key($this->taxonomy_slug) ) )
			{
				// Create labels based on the provided name and plural
				$default_labels = array(
	    			'name'              => $this->__( $this->taxonomy_plural ), // name of the custom taxonomy
	    			'singular_name'     => $this->__( $this->taxonomy_title ), // single taxonomy name
	    			'search_items'      => $this->__( 'Search '. $this->taxonomy_plural ), // search title for taxomony
	    			'all_items'         => $this->__( 'All '. $this->taxonomy_plural ), // all title for taxonomies
	    			'parent_item'       => $this->__( 'Parent '. $this->taxonomy_title ), // parent title for taxonomy
	    			'parent_item_colon' => $this->__( 'Parent '. $this->taxonomy_title .':' ), // parent taxonomy title
	    			'edit_item'         => $this->__( 'Edit '. $this->taxonomy_title ), // edit custom taxonomy title
	    			'update_item'       => $this->__( 'Update '. $this->taxonomy_title ), // update title for taxonomy
	    			'add_new_item'      => $this->__( 'Add New '. $this->taxonomy_title ), // add new title for taxonomy
	    			'new_item_name'     => $this->__( 'New '. $this->taxonomy_title .' Name' ), // name title for taxonomy
					'menu_name'         => $this->__( $this->taxonomy_title ),
	    		);
				$labels = array_merge($default_labels, $this->taxonomy_labels);
				
				// Setup default arguments
				$default_args = array(
		    		'labels'            => $labels,
		    		'hierarchical'      => true, // if this is true it acts like categories
		    		'public'            => true,
		    		'show_ui'           => true,
					'show_in_nav_menus' => true,
		    		'query_var'         => true,
					'_builtin'          => false,
					'rewrite'           => array( 'slug' => $this->taxonomy_slug ),
				);
				$args = array_merge($default_args, $this->taxonomy_args);
				
				#register_taxonomy( $this->taxonomy_slug, $post_type, $args );
				add_action( 'init',
					function() use( $slug, $post_type, $args ) {
						register_taxonomy( $slug, $post_type, $args );
					}
				);
			}
			else
			{
				#register_taxonomy_for_object_type( $this->taxonomy_slug, $post_type );
				add_action( 'init',
					function() use( $slug, $post_type ) {
						register_taxonomy_for_object_type( $slug, $post_type );
					}
				);
			}	
		}
	}
	
	public function setup_taxonomy( $taxonomy )
	{
		if ( is_array($taxonomy) )
		{
			if (! empty($taxonomy['name']) )
			{
				$taxonomy_args   = (! empty($taxonomy['args'])   ? $taxonomy['args']   : array());
				$taxonomy_labels = (! empty($taxonomy['labels']) ? $taxonomy['labels'] : array());
				$this->add_taxonomy($taxonomy['name'], $taxonomy_args, $taxonomy_labels);
			}
		}
		elseif ( is_string($taxonomy) )
		{
			$this->add_taxonomy($taxonomy);
		}
	}
}

catapost_post_type::get_instance();

function catapost_post_type( $post_type, $args=array() )
{
	
	if (! empty($args['taxonomies']) )
	{
		$taxonomies = $args['taxonomies'];
		unset($args['taxonomies']);
	}
	
	if (! empty($args['taxonomy']) )
	{
		$taxonomy = $args['taxonomy'];
		unset($args['taxonomy']);
	}
	
	$catapost_post_type = new catapost_post_type();
	$catapost_post_type->add_post_type( $post_type, $args );
	
	if (! empty($taxonomies) && is_array($taxonomies) )
	{
		foreach ( $taxonomies as $taxonomy )
		{
			$catapost_post_type->setup_taxonomy($taxonomy);
		}
	}
	
	if (! empty($taxonomy) )
	{
		$catapost_post_type->setup_taxonomy($taxonomy);
	}
	
	return $catapost_post_type;
}
