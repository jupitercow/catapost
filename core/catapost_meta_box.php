<?php

/**
 * Meta Box Manager
 *
 * @package catapost
 * @subpackage catapost_meta_box
 */

class catapost_meta_box extends catapost
{
	var $settings,
		$fields,
		$field_objects=array();
	
	static function get_instance()
	{
		static $instance;
		return $instance ? $instance : $instance = new self;
	}
	
	private function __clone() {}
	
	public function __construct()
	{
		parent::__construct();
		
		// Make sure field class is loaded
		$this->load_parent_field();
		
		// Add an action to show fields
		if (! has_action( $this->prefix .'_show_field' ) )
		{
			add_action( $this->prefix .'_show_field', array($this, 'show_field') );
		}
	}
	
	/**
	 * Attach a meta box to the post type
	 */
	public function add_meta_box( $args=array() )
	{
		$defaults = array(
			'id'        => false,
			'title'     => false,
			'post_type' => 'post',
			'context'   => 'normal',
			'priority'  => 'default',
			'fields'    => array()
		);
		$args = array_merge( $defaults, $args );
		
		if ( empty($args['title']) )
		{
			$args['title'] = $this->title_format($args['post_type']) .' '. $this->__('Settings');
			if ( empty($args['id']) ) $args['id'] = 'default';
		}
		elseif ( empty($args['id']) )
		{
			$args['id'] = $this->key_format($args['title']);
		}
		
		$args['post_type'] = $this->key_format($args['post_type']);
		
		extract($args);
		
		$this->load_parent_field();
		
		$this->settings = $args;
		foreach ( $fields as $field )
		{
			$this->add_field($field);
		}
		
		$callback = $this;
		
		add_action(
			'add_meta_boxes',
			function() use( $id, $title, $callback, $post_type, $context, $priority )
			{
				add_meta_box(
					$id,
					$title,
					array($callback, 'show_meta_box'),
					$post_type,
					$context,
					$priority
				);
			}
		);
		
		// Add a front end action for this metabox
		add_action( $this->prefix .'_meta_boxes_'. $this->settings['post_type'] .'_'. $this->settings['id'], array($this, 'show_meta_box'), 10, 2 );
		// Add to an action to get all meta boxes for this post type as well
		add_action( $this->prefix .'_meta_boxes_'. $this->settings['post_type'], array($this, 'show_meta_box'), 10, 2 );
		// Save fields
		add_action( 'save_post', array($this, 'save_post') );
	}
	
	/**
	 * Show the metabox form
	 */
	public function show_meta_box( $show_default_value=true, $post_id=0 )
	{
		// Add nonce for verification
		$this->nonce_field( __CLASS__ .'-'. $this->settings['post_type'] .'-'. $this->settings['id'],  $this->settings['post_type'] .'-'. $this->settings['id'] );
		
		// Begin the field output
		if (! empty($this->fields) && is_array($this->fields) )
		{
			?> 
			<div class="<?php echo $this->html_prefix; ?>-form <?php echo $this->html_prefix; ?>-form-meta <?php echo $this->html_prefix; ?>-form-<?php echo $this->settings['id']; ?>">
				
				<?php if ( isset($this->show_title) && $this->show_title ) echo "<h2>". $this->settings['title'] ."</h2>"; ?>
				
				<?php
				foreach ( $this->fields as $field )
				{
					$field['show_default_value'] = $show_default_value;
					$field['post_id'] = $post_id;
					$this->show_field($field);
				}
				?>
				
			</div>
			<?php
		}
	}
	
	public function load_parent_field()
	{
		if (! class_exists($this->field_namespace .'field') )
		{
			$class_path = $this->directory( 'core/field.php' );
			if ( is_file($class_path) ) require_once( $class_path );
		}
	}
	
	/**
	 * Load & Set Up Field Classes as they are needed.
	 *
	 * Load a single field class when it is requested, and store it in case other fields are the same type
	 * @return void
	 */
	public function load_field_class( $type )
	{
		$directory  = $this->directory( 'fields/' );
		$class_path = $directory . $type .'.class.php';
		
		if ( is_file($class_path) )
		{
			require_once( $class_path );
			$named_class = $this->field_namespace . $type;
			$this->field_objects[$type] = new $named_class;
		}
	}
	
	/**
	 * Get the class if loaded or load it if not
	 */
	public function get_field_class( $type )
	{
		if ( empty($this->field_objects[$type]) )  $this->load_field_class($type);
		if (! empty($this->field_objects[$type]) ) return $this->field_objects[$type];
		return false;
	}
	
	/**
	 * Add a meta field to the post type
	 */
	public function add_field( $args=array() )
	{
		$defaults = array(
			'type'        => 'text',
			'settings'    => array()
		);
		$args = array_merge( $defaults, $args );
		
		if (! empty($args['name']) )
		{
			// Load the field class
			$field_class = $this->get_field_class($args['type']);
			
			// Now the class should exist, so lets use it, or a class didn't load, and we can discard this, there is no class for the requested field
			if ( is_object($field_class) )
			{
				// Setup id/name attribute for field
				$meta_key = $field_class->meta_key($args['name']);
				
				// Save field settings for use in showing and saving
				$this->fields[$meta_key] = $args;
			}
		}
	}
	
	/**
	 * Show Meta Field
	 *
	 * Loads the class if it hasn't been loaded yet.
	 */
	public function show_field( $args=array() )
	{
		$type = ( isset($args['type']) ) ? $args['type'] : 'text';
		$method = __FUNCTION__;
		
		// Attempt to load field if it doesn't exist
		if ( empty($this->field_objects[$type]) ) $this->load_field_class($type);
		
		// Make sure field class exists and it has the proper method
		if (! empty($this->field_objects[$type]) && has_action( $this->prefix .'_'. $method .'_'. $type ) )
		{
			do_action( $this->prefix .'_'. $method .'_'. $type, $args );
		}
	}
		
	/**
	 * Listens for when the post type is being saved
	 */
	public function save_post( $post_id )
	{
		$post = get_post( $post_id );
		
		$method = 'save_field';
		
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && $post->post_type == $this->settings['post_type'] )
		{
			// Verify nonce
			if ( $this->verify_nonce( $post->post_type .'-'. $post->ID, __CLASS__ .'-'. $post->post_type .'-'. $post->ID ) ) return $post->ID;
			
			// Check autosave
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post->ID;
			
			// Check permissions
			if     ( 'page' == $post->post_type && ! current_user_can('edit_page', $post->ID) ) return $post->ID;
			elseif ( 'page' != $post->post_type && ! current_user_can('edit_post', $post->ID) ) return $post->ID;
			
			// Loop through fields and save the data
			foreach ( $this->fields as $field )
			{
				$type = $field['type'];
				$name = $field['name'];
				
				if (! empty($this->field_objects[$type]) && has_action($this->prefix .'_'. $method) )#method_exists($this->field_objects[$type], $method) )
				{
					call_user_func_array( array($this->field_objects[$type], $method), array($post->ID, $field['name']) );
					#do_action( $this->prefix .'_'. $method, $post->ID, $name );
				}
			}
		}
	}
}

catapost_meta_box::get_instance();

function catapost_meta_box( $post_type, $args=array() )
{
	if ( is_array($post_type) )
		$args = $post_type;
	else
		$args['post_type'] = $post_type;
	
	$catapost_meta_box = new catapost_meta_box();
	$catapost_meta_box->add_meta_box($args);
}
