<?php
/**
 * Post Save and Update from the Front End
 *
 * Will create new posts when 'create' is the designated action. Otherwise, if an existing ID is provided, will update existing.
 *
 * @package catapost
 * @subpackage catapost_front_end
 */

class catapost_front_end extends catapost
{
	var $post,
		$action,
		$nl,
		$messages=array(),
		$strings=array(),
		$field_classes=array(),
		$errors=array();
	
	static function get_instance()
	{
		static $instance;
		return $instance ? $instance : $instance = new self;
	}
	
	private function __clone() {}
	
	public function __construct()
	{
		parent::__construct();
		
		$this->strings = array(
			'added'            => $this->__( "Created" ),
			'updated'          => $this->__( "Updated" ),
			'err_post_title'   => $this->__( "Please add a title" ),
			'err_post_content' => $this->__( "Please add some content" ),
			'err_nonce'        => $this->__( "Your request could not be processed: Invalid NONCE" ),
			'err_permission'   => $this->__( "You do not have permission to do that" ),
			'err_generic'      => $this->__( "There was a problem making updates" )
		);
		$this->nl = "<br />\n";
		
		add_action( 'wp_loaded', array($this, 'manage_post') );
	}
	
	/**
	 * Set Up Post
	 *
	 * Get all the basic post info from $_POST and setup any other info we will need
	 *
     * @return void
	 */
	public function setup_post_data()
	{
		$this->post['ID']        = ( isset($_POST['post_ID']) )   ? $_POST['post_ID']   : 0;
		$this->post['post_type'] = ( isset($_POST[$this->prefix .'_post_type']) ) ? $_POST[$this->prefix .'_post_type'] : 'post';
		
		if ( isset($_POST[$this->prefix .'_post_title']) ) $this->post['post_title']   = $_POST[$this->prefix .'_post_title'];
		if ( isset($_POST[$this->prefix .'postcontent']) ) $this->post['post_content'] = $_POST[$this->prefix .'postcontent'];
	}
	
	/**
	 * Manage Post
	 *
	 * Checks to see if if a post should be added or updated
	 *
     * @return boolean true if all goes well and false if there are errors
	 */
	public function manage_post()
	{
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && ! empty($_POST['action']) )
		{
			$this->setup_post_data();
			
			if ( 'create' == $_POST['action'] )
			{
				$this->add_post();
			}
			elseif (! empty($this->post['ID']) )
			{
				$this->update_post();
			}
			else
			{
				$this->add_message('err_generic');
				$this->errors[$this->post['post_type']][] = 'err_generic';
			}
			
			/**/
			if (! empty($_POST[$this->meta_key('redirect')]) )
			{
				$redirect = $_POST[$this->meta_key('redirect')];
				
				if ( 'post' == $redirect )
				{
					$redirect = get_permalink($this->post['ID']);
				}
				elseif ( 'home' == $redirect )
				{
					$redirect = home_url();
				}
				else
				{
					if ( false === strpos($redirect, 'http://') && false === strpos($redirect, 'https://') )
					{
						$redirect = home_url($redirect);
					}
					else
					{
						$redirect = $redirect;
					}
				}
				
				wp_redirect( $redirect );
				exit;
			}
			/**/
		}
	}
	
	/**
	 * Add Post
	 *
	 * Verifies that a post is ready to be added, and then adds it
	 *
     * @return void
	 */
	public function add_post()
	{
		if ( $this->verify_has_content() && $this->verify_permission() && $this->verify_nonce_message() )
		{
			$user = wp_get_current_user();
			
			$this->post['post_author'] = $user->ID;
			$this->post['post_status'] = 'publish';
			
			$this->post = apply_filters( $this->prefix .'_'. __FUNCTION__, $this->post );
			
			$this->post['ID'] = wp_insert_post($this->post);
		}
	}
	
	/**
	 * Update Post
	 *
	 * Verifies that an existing post is ready to be updated and then updates it
	 *
     * @return void
	 */
	public function update_post()
	{
		if ( $this->verify_has_content() && $this->verify_permission() && $this->verify_nonce_message() )
		{
			$this->post = apply_filters( $this->prefix .'_'. __FUNCTION__, $this->post );
			
			wp_update_post($this->post);
		}
	}
	
	
	/**
     * Verify: NONCE
     *
     * Checks submitted nonce to make sure it is correct. Sets an error if it isn't and returns false.
     *
     * @return boolean true if correct, false if something is wrong
     */
    public function verify_nonce_message()
    {
    	/**/
        if (! $this->verify_nonce(__CLASS__ .'-'. $this->post['post_type'] .'-'. (isset($_POST['action']) ? $_POST['action'] :'') .'_'. $this->post['ID'], $this->post['post_type']) )
        {
        	$this->add_message('err_nonce');
        	$this->errors[$this->post['post_type']][] = 'err_nonce';
            return false;
        }
        /**/
        return true;
    }
	
	/**
	 * Verify: User Permission
	 *
	 * Checks if user can edit_post. If existing post, checks if user can edit it.
	 *
     * @return boolean true if correct, false if something is wrong
	 */
	public function verify_permission()
	{
		/**/
		if ( 0 == $this->post['ID'] )
		{
			if ( 'page' == $this->post['post_type'] )
				$test_type = 'publish_pages';
			else
				$test_type = 'publish_posts';
			
			$current_user_can = (! current_user_can($test_type) ) ? false : true;
		}
		else
		{
			if ( 'page' == $this->post['post_type'] )
				$test_type = 'edit_page';
			else
				$test_type = 'edit_post';
			
			$current_user_can = (! current_user_can($test_type, $this->post['ID']) ) ? false : true;
		}
		
		if (! $current_user_can )
		{
			$this->add_message('err_permission');
			$this->errors[$this->post['post_type']][] = 'err_permission';
			return false;
		}
		/**/
		return true;
	}
	
	/**
	 * Verify: Post Has Title
	 *
	 * Used when creating a new post to make sure a title was submitted. Posts are not created without a title
	 *
     * @return boolean true is has title, false if missing
	 */
	public function verify_has_content()
	{
		$output = true;
		
		if ( isset($this->post['post_title']) && empty($this->post['post_title']) )
		{
			$output = false;
			$error  = 'post_title';
		}
		elseif (! isset($this->post['post_title']) && isset($this->post['post_content']) && empty($this->post['post_content']) )
		{
			$output = false;
			$error  = 'post_content';
		}
		
		$output = apply_filters( $this->prefix .'_verify_has_content', $output, $this->post );
		
		if (! $output )
		{
			$this->add_message('err_'. $error);
			$this->errors[$this->post['post_type']][] = $error;
		}
		
		return $output;
	}
	
	public function add_message( $text )
	{
		// Set post type depending on whether this is processing or creating a form
		$post_type = '';
		if (! empty($this->post['post_type']) )
			$post_type = $this->post['post_type'];
		elseif (! empty($this->form['post_type']) )
			$post_type = $this->form['post_type'];
		
		if ( empty($text) ) return;
		
		if (! empty($this->strings[$text]) )
		{
			if ( empty($this->messages[$post_type][$text]) ) $this->messages[$post_type][$text] = $this->strings[$text];
		}
		elseif (! in_array($text, $this->messages[$post_type]) )
		{
			$this->messages[$post_type][] = $text;
		}
	}
	
	public function show_messages( $single_message='' )
	{
		$output = '';
		
		if (! empty($this->messages[$this->form['post_type']]) || ! empty($single_message) )
		{
			$output .= '<div class="'. $this->prefix .'-message message'. (empty($single_message) && ! empty($this->errors) ? ' '. $this->prefix .'-message-errors message-errors' : '') .'">';
				$output .= '<p>';
				
				if (! empty($single_message) )
				{
					$output .= $single_message ."<br />\n";
				}
				else
				{
					foreach ( $this->messages[$this->form['post_type']] as $message )
					{
						$output .= $message ."<br />\n";
					}
				}
				$output .= '</p>';
			$output .= '</div>';
		}
		
		return $output;
	}
	
	public function form_header()
	{
		$user = wp_get_current_user();
		
		$output  = '';
		$output .= '<div class="'. $this->prefix .'-form '. $this->prefix .'-form-post '. $this->prefix .'-form-'. $this->form['post_type'] .' front-end-form'. (! empty($this->errors[$this->form['post_type']]) ? ' front-end-form-errors' : '') .'">';
		$output .= $this->show_messages();
		
		$form_name = $this->prefix .'-'. $this->form['post_type'] .'-'. $this->form['action'];
		$output .= '<form method="post" id="'. $form_name .'" name="'. $form_name .'">';
		$output .= '<input type="hidden" name="user_ID"   value="'. $user->ID .'" />';
		$output .= '<input type="hidden" name="action"    value="'. $this->form['action'] .'" />';
		$output .= '<input type="hidden" name="'. $this->prefix .'_post_type" value="'. $this->form['post_type'] .'" />';
		$output .= '<input type="hidden" name="post_ID"   value="'. $this->form['post_id'] .'" />';
		
		echo $output;
		$this->nonce_field( __CLASS__ .'-'. $this->form['post_type'] .'-'. $this->form['action'] .'_'. $this->form['post_id'], $this->form['post_type'] );
	}
	
	public function form_footer()
	{
		$output  = '';
		$output .= '<input type="submit" name="submit" id="submit" class="button-primary button" value="'. $this->form['submit_text'] .'">';
		$output .= '</form>';
		$output .= '</div>';
		
		echo $output;
	}
	
	public function form_post( $show_default_value=true )
	{
		$this->form_post_title($show_default_value);
		$this->form_post_content($show_default_value);
	}
	
	public function form_post_title( $post_id=0, $show_default_value=false )
	{
		if (! empty($post_id) && is_numeric($post_id) )
		{
			$post = get_post($post_id);
		}
		else 
		{
			global $post;
		}
		
		$args = array(
			'type'     => 'text',
			'meta_key' => $this->prefix .'_post_title',
			'name'     => 'Title'
		);
		$args['value'] = ( $show_default_value ) ? $post->post_title : false;
		
		do_action( $this->prefix .'_show_field', $args );
	}
	
	public function form_post_content( $post_id=0, $show_default_value=false, $type=true, $settings=array() )
	{
		if (! empty($post_id) && is_numeric($post_id) )
		{
			$post = get_post($post_id);
		}
		else 
		{
			global $post;
		}
		
		$args = array(
			'type'     => (! empty($type) && is_string($type) ) ? $type : 'wysiwyg',
			'meta_key' => $this->prefix .'postcontent',
			'name'     => 'Description',
			'settings' => $settings
		);
		$args['value'] = ( $show_default_value ) ? $post->post_content : false;
		
		do_action( $this->prefix .'_show_field', $args );
	}
	
	public function setup_form_data( $args=array() )
	{
		global $post;
		
		$defaults = array(
			'action'              => 'edit',
			'post_id'             => 0,
			'post_type'           => 'post',
			'post_title'          => true,
			'post_content'        => 'wysiwyg',
			'editor_settings'     => array(),
			'meta_boxes'          => true,
			'submit_text'         => false,
			'show_default_value'  => true,
			'meta_key'            => '',
			'meta_show_title'     => false,
			'fields'              => false
		);
		$args = array_merge( $defaults, $args );
		
		if ( 'create' == $args['action'] )
		{
			$args['show_default_value'] = false;
			if (! $args['submit_text'] ) $args['submit_text'] = "Create";
		}
		elseif (! $args['submit_text'] )
		{
			$args['submit_text'] = "Save";
		}
		
		// Setup post_type
		$args['post_type'] = $this->key_format($args['post_type']);
		
		// store the form info
		$this->form = $args;
		
		return $args;
	}
	
	/**
	 * Used to create a form on the front end to edit a page, post, or custom post type.
	 */
	public function create_form( $args=array() )
	{
		$args = $this->setup_form_data($args);
		
		// extract form info to variables
		extract($args);
		
		// Start the form
		$this->form_header();
		
		if ( $post_title || $post_content )
		{
			?> 
			<div class="front-end-title-content front-end-post">
				<?php
				// Add field to update title if needed
				if ( $post_title )   $this->form_post_title($post_id, $show_default_value);
				
				// Add field to update post_content if needed
				if ( $post_content ) $this->form_post_content($post_id, $show_default_value, $post_content, $editor_settings);
				?> 
			</div>
			<?php
		}
		
		if (! empty($redirect) )
		{
			$fields['redirect'] = array(
				'type'   => 'hidden',
				'value'  => $redirect
			);
		}
		
		// Try to show meta boxes using the meta_box class
		if (! empty($meta_boxes) )
		{
			if ( true !== $meta_boxes )
			{
				if ( is_array($meta_boxes) )
				{
					foreach ( $meta_boxes as $meta_box )
					{
						do_action( $this->prefix .'_meta_boxes_'. $post_type .'_'. $meta_box, $show_default_value, $post_id );
					}
				}
				elseif ( is_string($meta_boxes) )
				{
					do_action( $this->prefix .'_meta_boxes_'. $post_type .'_'. $meta_boxes, $show_default_value, $post_id );
				}
			}
			else
			{
				do_action( $this->prefix .'_meta_boxes_'. $post_type .'_default', $show_default_value, $post_id );
			}
		}
		
		// Try to show fields using the meta_box class
		if (! empty($fields) && is_array($fields) )
		{
			foreach ( $fields as $key => $args )
			{
				$args['name'] = $key;
				if (! isset($args['show_default_value']) ) $args['show_default_value'] = $show_default_value;
				$args['post_id'] = $post_id;
				do_action( $this->prefix .'_show_field', $args );
			}
		}
		
		// End form (includes submit)
		$this->form_footer();
	}
}

catapost_front_end::get_instance();

/**
 * These functions should be used in your functions that register scripts/styles or an init action.
 *
 * Or they can be added above the get_header call on a page that has a form.
 *
 * @package front_end_post
 */
function catapost_form_head()
{
	$catapost = new catapost();
	$catapost->form_init();
}

/**
 * Used to create a form on the front end to edit a page, post, or custom post type.
 *
 * @package front_end_post
 */
function catapost_form( $args=array() )
{
	// Show the form
	$front_end_post = catapost_front_end::get_instance();
	$front_end_post->create_form( $args );
}
