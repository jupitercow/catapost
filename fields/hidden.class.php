<?php

/**
 * Hidden Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class hidden extends field
{
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value to populate with on load
     * @return void
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		$this->formblock_input($this->type, $meta_key, $value);
	}
}