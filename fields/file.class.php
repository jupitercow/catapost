<?php

/**
 * File Upload Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class file extends field
{
	public function __construct()
	{
		parent::__construct();
		
		$this->update_media_uploader();
	}
	
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $file_id the id of the file (post using attachment post_type) currently associated with this field
     * @param array $settings the settings for the image, currently preview_size
     * @return void
	 */
	public function create( $meta_key, $file_id=0, $settings=array() )
	{
		$file_info = $this->get_file_info($file_id);
		
		?> 
		<div class="<?php echo $this->prefix; ?>-file-uploader <?php echo $this->prefix; ?>-uploader clearfix<?php if (! empty($file_info['url']) ) echo ' active'; ?>">
			<?php $this->formblock_input('hidden', $meta_key, $file_id); ?>
			
			<div class="has-file">
				<img class="file-icon" src="<?php echo $file_info['icon']; ?>" alt="" />
				<ul>
					<li class="file-name">
						<?php echo $file_info['name']; ?>
					</li>
					<li><a class="remove-file" href="#"><?php $this->_e('Remove'); ?></a></li>
					<li><a class="edit-file" href="#"><?php $this->_e('Edit'); ?></a></li>
				</ul>
			</div>
			
			<div class="no-file">
				<p><?php $this->_e('No file selected'); ?> <input type="button" class="button add-file" value="<?php $this->_e('Add File'); ?>" /></p>
			</div>
		</div>
		<?php
	}
	
	/**
	 * Get File Info
	 *
	 * Info on non-image files including mime type
	 *
     * @param int $id the id of the attachment post
     * @return array image info
	 */
	public function get_file_info( $id )
	{
		if ( empty($id) || ! is_numeric($id) ) return;
		
		$output = array();
		$output['url']  = wp_get_attachment_url($id);
		$output['name'] = $this->get_file_name_from_url($output['url']);
		$output['type'] = get_post_mime_type($id);
		$output['icon'] = wp_mime_type_icon($id);
		
		return $output;
	}
	
	/**
	 * Get File Name From a URL String
	 *
     * @param string $url the full url to extract the filename from
     * @return string file name
	 */
	public function get_file_name_from_url( $url )
	{
		return substr(strrchr($url, '/'), 1);
	}
	
	public function field_init()
	{
		$this->update_media_uploader();
		#add_action( 'admin_head', array(&$this, 'field_print_scripts') );
	}
	
	public function update_media_uploader() {
		global $pagenow;
		
		if ( 'media-upload.php' == $pagenow || 'async-upload.php' == $pagenow ) {
			add_filter( 'gettext',              array(&$this, 'replace_insert_text'), 1, 3 );
			add_filter( 'media_upload_tabs',    array(&$this, 'media_upload_tabs') );
			add_filter( 'media_send_to_editor', array(&$this, 'media_send_to_editor'), 20, 3 );
		}
	}
	
	public function replace_insert_text( $translated_text, $text, $domain )
	{
		if ( 'Insert into Post' == $text )
		{
			if ( false !== strpos(wp_get_referer(), $this->prefix .'_type') )
			{
				return $this->__('Select File');
			}
		}
		return $translated_text;
	}
	
	public function media_upload_tabs( $tabs )
	{
		if (! empty($_GET[$this->prefix .'_type']) )
		{
			unset($tabs['type_url'], $tabs['gallery']);
		}
		return $tabs;
	}
	
	public function media_send_to_editor( $html, $id, $caption )
	{
		/**/if ( false !== strpos(wp_get_referer(), $this->prefix .'_type') )
		{
			$mime  = get_post_mime_type($id);
			$icon  = wp_mime_type_icon($id);
			$link  = wp_get_attachment_url($id);
			$title = get_the_title($id);
			
			if ( false === strpos($mime, 'image') )
			{
				$html = '<a data-mime="'. $mime .'" class="'. $this->prefix .'-file-'. $id .'" href="'. $link .'" title="'. $title .'">'. $title .' <img src="'. $icon .'" alt="" /></a>';
			}
		}/**/
		return $html;
	}
	
	/**
	 * Setup Scripts & Styles
	 *
	 * @return void
	 */
	function field_scripts_and_styles()
	{
		wp_enqueue_script(array(
			'jquery',
			'thickbox',
			'media-upload',
			'catapost-js'
		));
	}
	
}