<?php

/**
 * Textarea Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class textarea extends field
{
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value to populate with on load
     * @param array $settings optional rows attribute, default is "4"
     * @return void
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		if ( empty($settings['rows']) ) $settings['rows'] = 4;
		
		?> 
		<textarea<?php $this->formblock_nameid($meta_key); ?> class="<?php echo $this->type; ?>" rows="<?php echo esc_attr($settings['rows']); ?>"><?php echo esc_textarea($value); ?></textarea>
		<?php
	}
}