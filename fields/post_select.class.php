<?php

/**
 * Post Select Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class post_select extends field
{
	var $default_option_text,
		$field_id;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->default_option_text = ' - '. $this->__('Select') .' - ';
	}
	
	/**
	 * Setup Default/Null Text if provided
	 *
     * @param string $string the text for the default/null option
     * @return void
	 */
	public function set_default_option_text( $string )
	{
		$this->default_option_text = esc_html__($string, $this->domain);
	}
	
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value previously selected
     * @param array $settings settings for the field, eg: "allow_null" will add a uniform null value option
     * @return void
	 */
	public function create( $meta_key, $value=false, $settings=array() )
	{
		if (! empty($settings['default_option_text']) ) $this->set_default_option_text($settings['default_option_text']);
		
		// Setup multiselect if requested
		$name_key = esc_attr($meta_key);
		$multiple = '';
		if (! empty($settings['multiselect']) )
		{
			$name_key = esc_attr($meta_key) . '[]';
			$multiple = 'multiple="multiple"';
		}
		
		// Setup args for get_posts
		$args = (! empty($settings['args']) ) ? $settings['args'] : array();
		// Add default posts_per_page to be unlimitted instead of wp default
		if ( empty($args['posts_per_page']) ) $args['posts_per_page'] = -1;
		
		$items = get_posts($args);
		
		$this->field_id = $meta_key .'-'. rand();
		?> 
		<select id="<?php echo esc_attr($this->field_id); ?>" name="<?php echo $name_key; ?>" class="<?php echo $this->type; ?>"<?php echo $multiple; ?><?php $this->formblock_value($value); ?> data-placeholder="<?php echo $this->default_option_text; ?>">
			<?php 
			if ( empty($settings['allow_null']) && empty($settings['multiselect']) ) $this->formblock_option($meta_key, (! $value ? 'null' : ''),  $this->default_option_text, 'null');
			
			if (! empty($items) && is_array($items) )
			{ 
				foreach( $items as $item )
				{
					$option_name = ( is_array($args['post_type']) ) ? apply_filters('the_title', $option_name = $item->post_type.': '.$item->post_title) : apply_filters('the_title', $item->post_title);
					
					$this->formblock_option($meta_key, $value, $option_name, $item->ID);
				}
			}
			?> 
		</select>
		<?php
		
		$js = ( isset($settings['js']) ) ? $settings['js'] : array();
		$this->create_script($meta_key, $value, $js);
	}
	
	/**
	 * Javascript
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value of previously selected
     * @param array $$settings the settings for the slider: min, max, step
     * @return void
	 */
	public function create_script( $meta_key, $value, $settings=array() )
	{
		?> 
		<script type="text/javascript">
		jQuery(document).ready(function($){
			if ( jQuery().select2 )
			{
				jQuery("#<?php echo esc_js($this->field_id); ?>").select2(
					<?php 
					if (! empty($settings) && is_array($settings) )
					{
						$options = '';
						foreach ( $settings as $setting => $value )
						{
							$options .= "'". $setting ."':'". $value ."',";
						}
						echo '{'. rtrim($options, ',') .'}';
					}
					?> 
				);
			}
		});
		</script>
		<?php
	}
	
	/**
	 * Setup Scripts & Styles
	 *
	 * @return void
	 */
	function field_scripts_and_styles()
	{
		wp_register_style( 'select2-css', $this->directory_uri( 'css/select2.css' ), array(), '3.2', 'all' );
		wp_enqueue_style(array(
			'select2-css'
		));
		
		wp_register_script( 'select2-js', $this->directory_uri( 'js/select2.min.js' ), array( 'jquery' ), '3.2', true );
		wp_enqueue_script(array(
			'jquery',
			'select2-js'
		));
	}
}