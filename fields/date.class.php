<?php

/**
 * Date Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class date extends field
{
	public function __construct()
	{
		parent::__construct();
		
		$this->default_option_text = ' - '. $this->__('Select a Taxonomy') .' - ';
		add_action( 'wp_head', array($this, 'set_date_format_header') );
		add_action( 'admin_head', array($this, 'set_date_format_header') );
	}
	
	public function set_date_format_header()
	{
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			$.datepicker.setDefaults({
				dateFormat:  '<?php echo $this->dateStringToDatepickerFormat(get_option('date_format')); ?>',
				altFormat:   '@',
				gotoCurrent: true
			});
		});
		</script>
		<?php
	}
	
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value of previously selected
     * @param array $settings the settings for the slider: min, max, step
     * @return void I was thinking of something like qTranslate.
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		// if value is a timestamp, format to the date_format option
		if (! empty($value) && is_numeric($value) ) $value = date(get_option('date_format'), $value);
		// Set up the value
		if ( empty($value) && isset($settings['min']) ) $value = $settings['min'];
		
		$this->formblock_input('text', $meta_key, $value);
		
		$js = ( isset($settings['js']) ) ? $settings['js'] : array();
		#if ( empty($js['gotoCurrent']) ) $js['gotoCurrent'] = true;
		#if ( empty($js['dateFormat']) ) $js['dateFormat'] = $this->dateStringToDatepickerFormat(get_option('date_format'));
		#if ( empty($js['altFormat']) ) $js['altFormat'] = "@";
		$this->create_script($meta_key, $value, $js);
	}
	
	/**
	 * Javascript
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value of previously selected
     * @param array $settings the settings for the slider: min, max, step
     * @return void
	 */
	function create_script( $meta_key, $value, $settings=array() )
	{
		$random_id = esc_js($meta_key) .'-'. rand();
		?> 
		<script type="text/javascript">
		jQuery(document).ready(function($){
			if ( $.datepicker )
			{
				$("#<?php echo esc_js($meta_key); ?><?php echo ($value ? "[value='$value']" : ''); ?>").attr('id', '<?php echo $random_id; ?>');
				$("#<?php echo $random_id; ?>").datepicker(
					<?php 
					if (! empty($settings) && is_array($settings) )
					{
						$options = '';
						foreach ( $settings as $setting => $value )
						{
							$options .= "'". $setting ."':'". $value ."',";
						}
						echo '{'. rtrim($options, ',') .'}';
					}
					?> 
				);
			}
		});
		</script>
		<?php
	}
	
	/**
	 * Setup Scripts & Styles
	 *
	 * @return void
	 */
	function field_scripts_and_styles()
	{
		wp_enqueue_style(array(
			'catapost-jquery-ui-theme'
		));
		
		wp_enqueue_script(array(
			'jquery-ui-core',
			'jquery-ui-datepicker'
		));
	}
	
	function dateStringToDatepickerFormat( $dateString )
	{
		$pattern = array(
			
			//day
			'd',		//day of the month
			'j',		//3 letter name of the day
			'l',		//full name of the day
			'z',		//day of the year
			
			//month
			'F',		//Month name full
			'M',		//Month name short
			'n',		//numeric month no leading zeros
			'm',		//numeric month leading zeros
			
			//year
			'Y', 		//full numeric year
			'y'		    //numeric year: 2 digit
		);
		$replace = array(
			'dd','d','DD','o',
			'MM','M','m','mm',
			'yy','y'
		);
		foreach ( $pattern as &$p )
		{
			$p = '/'.$p.'/';
		}
		return preg_replace($pattern,$replace,$dateString);
	}
}