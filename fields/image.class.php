<?php

/**
 * Image Upload Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class image extends field
{
	function __construct()
	{
		parent::__construct();
		
		$this->update_media_uploader();
	}
	
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $file_id the id of the image (post using attachment post_type) currently associated with this field
     * @param array $settings the settings for the image, currently preview_size
     * @return void
	 */
	function create( $meta_key, $file_id=0, $settings=array() )
	{
		$file_info = array();
		
		$preview_size = (! empty($settings['preview_size']) ) ? $settings['preview_size'] : 'thumbnail';
		
		if (! empty($file_id) && is_numeric($file_id) )
		{
			$file_info['src'] = wp_get_attachment_image_src($file_id, $preview_size);
			$file_info['url'] = $file_info['src'][0];
		}
		
		?> 
		<div class="<?php $this->the_prefix(); ?>-image-uploader <?php $this->the_prefix(); ?>-uploader clearfix<?php if (! empty($file_info['url']) ) echo ' active'; ?>" data-preview_size="<?php echo esc_attr($preview_size); ?>">
			<?php $this->formblock_input('hidden', $meta_key, $file_id); ?>
			
			<div class="has-file">
				<img class="preview" src="<?php echo $file_info['url']; ?>" alt="" />
				<ul>
					<li><a class="remove-file" href="#"><?php $this->_e('Remove'); ?></a></li>
					<li><a class="edit-file" href="#"><?php $this->_e('Edit'); ?></a></li>
				</ul>
			</div>
			
			<div class="no-file">
				<p><?php $this->_e('No image selected'); ?> <input type="button" class="button add-file" value="<?php $this->_e('Add Image'); ?>" /></p>
			</div>
		</div>
		<?php
	}
	
	function update_media_uploader() {
		global $pagenow;
		
		if ( 'media-upload.php' == $pagenow || 'async-upload.php' == $pagenow ) {
			add_filter( 'gettext',              array(&$this, 'replace_insert_text'), 1, 3 );
			add_filter( 'media_upload_tabs',    array(&$this, 'media_upload_tabs') );
		}
	}
	
	function replace_insert_text( $translated_text, $text, $domain )
	{
		if ( 'Insert into Post' == $text )
		{
			if ( false !== strpos(wp_get_referer(), $this->prefix .'_type') )
			{
				return $this->__('Select File');
			}
		}
		return $translated_text;
	}
	
	function media_upload_tabs( $tabs )
	{
		if (! empty($_GET[$this->prefix .'_type']) )
		{
			unset($tabs['type_url'], $tabs['gallery']);
		}
		return $tabs;
	}
	
	/**
	 * Setup Scripts & Styles
	 *
	 * @return void
	 */
	function field_scripts_and_styles()
	{
		wp_enqueue_script(array(
			'jquery',
			'thickbox',
			'media-upload',
			'catapost-js'
		));
	}
	
}