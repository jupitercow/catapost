<?php

/**
 * Checkbox Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class checkbox extends field
{
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional array() of previously selected values
     * @param array $options the options for the checkboxes
     * @return void
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		if (! empty($settings['options']) && is_array($settings['options']) )
		{
		?> 
		<ul class="<?php echo $this->type; ?>_list">
			<?php
			foreach( $settings['options'] as $option )
			{
				$this->formblock_box($this->type, $meta_key, $value, $option['name'], $option['value']);
			}
			?> 
		</ul>
		<?php
		}
	}
}