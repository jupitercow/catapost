<?php

/**
 * Slider Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class slider extends field
{
	function __construct()
	{
		parent::__construct();
		
		$this->field_init();
	}
	
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value of previously selected
     * @param array $settings the settings for the slider: min, max, step
     * @return void
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		echo '<div id="'. esc_attr($meta_key) .'-'. $this->type .'"></div>';
		
		// Set up the value
		if ( empty($value) ) $value = ( isset($settings['min']) ) ? $settings['min'] : 0;
		
		$this->formblock_input('text', $meta_key, $value);
		
		$js = ( isset($settings['js']) ) ? $settings['js'] : array();
		$this->create_script($meta_key, $value, $js);
	}
	
	/**
	 * Javascript
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value of previously selected
     * @param array $$settings the settings for the slider: min, max, step
     * @return void
	 */
	function create_script( $meta_key, $value, $settings=array() )
	{
		$random_id = esc_js($meta_key) .'-'. rand();
		?> 
		<script type="text/javascript">
		jQuery(document).ready(function($){
			if ( jQuery().slider )
			{
				jQuery("#<?php echo esc_js($meta_key); ?><?php echo ($value ? "[value='$value']" : ''); ?>").attr('id', '<?php echo $random_id; ?>');
				jQuery("#<?php echo $random_id; ?>").slider({
					<?php
					echo 'value: '. $value .',', 
						(isset($settings['min'])  ? 'min: '.  esc_js($settings['min'])  .',' : ''),
						(isset($settings['max'])  ? 'max: '.  esc_js($settings['max'])  .',' : ''),
						(isset($settings['step']) ? 'step: '. esc_js($settings['step']) .',' : ''); ?> 
					slide: function(event, ui){
						jQuery("#<?php echo esc_js($meta_key); ?>").val( ui.value );
					}
				});
			}
		});
		</script>
		<?php
	}
	
	/**
	 * Setup Scripts & Styles
	 *
	 * @return void
	 */
	function field_scripts_and_styles()
	{
		wp_enqueue_style(array(
			'catapost-jquery-ui-theme'
		));
		
		wp_enqueue_script(array(
			'jquery-ui-core',
			'jquery-ui-slider'
		));
	}
}