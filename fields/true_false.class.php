<?php

/**
 * True/False Field
 *
 * A boolean where checked is on and unchecked is off.
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class true_false extends field
{
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional array() of previously selected values
     * @param array $options the options for the checkboxes
     * @return void
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		?> 
		<ul class="checkbox_list <?php echo $this->type; ?>_list">
			<input<?php
				$default_value = 1;
				
				$this->formblock_type('checkbox');
				$this->formblock_nameid($meta_key);
				$this->formblock_value($default_value);
				$this->formblock_checked($value == $default_value);
			?> />
		</ul>
		<?php
	}
}