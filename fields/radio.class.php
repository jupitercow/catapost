<?php

/**
 * Radio Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class radio extends field
{
	/**
	 * Create Field
	 *
     * @param string $meta_key the id/name
     * @param string $value the optional value of previously selected
     * @param array $options the options for the radios
     * @return void
	 */
	function create( $meta_key, $value=array(), $settings=array() )
	{
		if (! empty($settings['options']) && is_array($settings['options']) )
		{
		?> 
		<ul class="radio_list radio vertical">
			<?php 
			foreach( $settings['options'] as $option )
			{
				$this->formblock_box('radio', $meta_key, $value, $option['name'], $option['value']);
			}
			?> 
		</ul>
		<?php
		}
	}
}