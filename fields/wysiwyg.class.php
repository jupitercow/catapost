<?php

/**
 * WYSIWYG Field
 * 
 * @package catapost
 * @subpackage field
 */

namespace catapost\field;

class wysiwyg extends field
{
	/**
	 * Create Field
	 *
	 * Mainly a wrapper for the wp_editor function
	 *
     * @param string $meta_key cannot contain anything but alphanumeric characters
     * @param string $value the optional value to populate with on load
     * @param array $settings settings for wp_editor to use. There are few defaults that are ideal for most meta fields
     * @return void
	 */
	function create( $meta_key, $value=false, $settings=array() )
	{
		// Add some default settings that can be overwritten
		if ( empty($settings) ) $settings = array('media_buttons' => false, 'quicktags' => false, 'teeny' => true);
		
		// Quick fix to make sure background is white, this might get moved into stylesheet later
		$settings['editor_css'] = (! empty($settings['editor_css']) ? rtrim($settings['editor_css'],'</style>') : '<style>') . '.wp-editor-container{background-color:white;width:100%;}</style>';
		
		rocketpak_wp_editor( $value, $meta_key, $settings );
	}
	
	/**
	 * Overwrite The Meta Key Creator
	 *
	 * wysiwyg cannot have any characters outside of alpha-numeric characters, so it has to be slimmed down here
	 *
     * @param string $name the name to use, probably a title
     * @return $string newly formatted title with prefix
	 */
	function meta_key( $name )
	{
		return $this->prefix . strtolower( str_replace( array('_','-'), '', $this->key_format($name) ) );
	}
}